/**
 * Utility classes for internal use by the API.
 */
@Internal
@ParametersAreNonnullByDefault package com.atlassian.scheduler.util;

import com.atlassian.annotations.Internal;

import javax.annotation.ParametersAreNonnullByDefault;