package com.atlassian.scheduler.config;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.util.Objects;
import java.util.TimeZone;

import static java.util.Objects.requireNonNull;

/**
 * The description of a {@link Schedule#forCronExpression(String, TimeZone) cron expression schedule}.
 *
 * @since v1.0
 */
@Immutable
@PublicApi
public final class CronScheduleInfo {
    private final String cronExpression;
    private final TimeZone timeZone;

    CronScheduleInfo(String cronExpression, @Nullable TimeZone timeZone) {
        this.cronExpression = requireNonNull(cronExpression, "cronExpression");
        this.timeZone = timeZone;
    }


    @Nonnull
    public String getCronExpression() {
        return cronExpression;
    }

    @Nullable
    public TimeZone getTimeZone() {
        return timeZone;
    }


    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CronScheduleInfo other = (CronScheduleInfo) o;
        return cronExpression.equals(other.cronExpression) &&
                Objects.equals(timeZone, other.timeZone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cronExpression, timeZone);
    }

    @Override
    public String toString() {
        final String timeZoneId = (timeZone != null) ? timeZone.getID() : null;
        return "CronScheduleInfo[cronExpression='" + cronExpression + "',timeZone=" + timeZoneId + ']';
    }
}
