package com.atlassian.scheduler.config;

import com.atlassian.annotations.PublicApi;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.Serializable;

import static java.util.Objects.requireNonNull;

/**
 * A wrapper to distinguish job runner keys from simple strings and to make it easier
 * to avoid confusing them with Job IDs
 *
 * @since v1.0
 */
@Immutable
@PublicApi
public final class JobRunnerKey implements Serializable, Comparable<JobRunnerKey> {
    private static final long serialVersionUID = 1L;

    /**
     * Wraps the provided string as a {@code JobRunnerKey}.
     * <p>
     * Although it is not necessary for correctness, it will usually make sense to create a single instance of
     * the {@code JobRunnerKey} and reuse it as a constant, as in:
     * </p>
     * <pre><code>
     *     private static final JobRunnerKey <strong>POP3_SERVICE</strong> = JobRunnerKey.of("com.example.plugin.Pop3Service");
     *
     *     // ...
     *
     *     private void registerJobRunner()
     *     {
     *         schedulerService.registerJobRunner(<strong>POP3_SERVICE</strong>, new Pop3JobRunner());
     *     }
     *
     *     private String scheduleJobWithGeneratedId(String cronExpression)
     *     {
     *         JobConfig jobConfig = JobConfig.forJobRunnerKey(<strong>POP3_SERVICE</strong>)
     *                  .withSchedule(Schedule.forCronExpression(cronExpression));
     *         return schedulerService.scheduleJobWithGeneratedId(jobConfig);
     *     }
     * </code></pre>
     *
     * @param key the job runner key, as a string
     * @return the wrapped job runner key
     */
    public static JobRunnerKey of(String key) {
        return new JobRunnerKey(key);
    }


    private final String key;

    private JobRunnerKey(final String key) {
        this.key = requireNonNull(key, "key");
    }


    @Override
    @SuppressWarnings("SimplifiableIfStatement")
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        return o != null
                && o.getClass() == getClass()
                && ((JobRunnerKey) o).key.equals(key);
    }

    @Override
    public int compareTo(final JobRunnerKey o) {
        return key.compareTo(o.key);
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    public String toString() {
        return key;
    }
}
