/**
 * API classes related to the configuration of scheduled jobs.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.config;

import javax.annotation.ParametersAreNonnullByDefault;