package com.atlassian.scheduler.util;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import static com.atlassian.scheduler.util.Safe.copy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

/**
 * @since v1.0
 */
public class SafeTest {
    @Test
    public void testCopyDateNull() {
        assertNull(copy((Date) null));
    }

    @Test
    public void testCopyDate() {
        final Date now = new Date();
        final Date copy = copy(now);
        assertEquals(now, copy);
        assertNotSame(now, copy);
    }

    @Test
    public void testCopyMapNull() {
        final Map<String, Serializable> map = copy((Map<String, Serializable>) null);
        assertTrue("Should be an ImmutableMap", map instanceof ImmutableMap);
    }

    @Test
    public void testCopyMapKnownImmutable() {
        final ImmutableMap<String, Serializable> map = ImmutableMap.<String, Serializable>builder()
                .put("Hello", 42L)
                .put("World", true)
                .build();
        assertSame(map, copy(map));
    }
}
