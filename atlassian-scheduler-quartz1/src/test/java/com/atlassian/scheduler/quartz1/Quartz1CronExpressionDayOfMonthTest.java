package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.CronExpressionDayOfMonthTest;
import org.junit.Ignore;
import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;

/**
 * @since v1.5
 */
public class Quartz1CronExpressionDayOfMonthTest extends CronExpressionDayOfMonthTest {
    public Quartz1CronExpressionDayOfMonthTest() {
        super(new Quartz1CronFactory());
    }

    @Override
    @Test
    public void testNearestWeekdayToLastDayOfMonthEdgeCase() {
        assertQuartzBug("Edge case - 2014/28/02 and 29W", "0 0 0 29W * ?", unsatisfiedBy(0, 0, 0, 28, 2, 2014));
        assertQuartzBug("Edge case - 2010/04/30 and 31W", "0 0 0 31W * ?", unsatisfiedBy(0, 0, 0, 30, 4, 2010));
    }

    @Ignore
    @Override
    @Test
    public void testOffsetFromLastDayOfMonth() {
        System.err.println("This feature is not supported in Quartz 1.x");
    }

    @Ignore
    @Override
    @Test
    public void testClosestWeekdayToOffsetFromLastDayOfMonth() {
        System.err.println("This feature is not supported in Quartz 1.x");
    }
}
