package com.atlassian.scheduler.quartz1;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;
import org.quartz.JobDetail;
import org.quartz.JobPersistenceException;
import org.quartz.Trigger;
import org.quartz.core.SchedulingContext;
import org.quartz.impl.jdbcjobstore.DriverDelegate;
import org.quartz.impl.jdbcjobstore.NoSuchDelegateException;
import org.quartz.utils.Key;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.quartz.impl.jdbcjobstore.Constants.STATE_MISFIRED;
import static org.quartz.impl.jdbcjobstore.Constants.STATE_WAITING;

public class Quartz1HardenedJobStoreTest {
    private static final Logger LOG = LoggerFactory.getLogger(Quartz1HardenedJobStoreTest.class);

    private final AtomicInteger callCount = new AtomicInteger();
    private final AtomicInteger throwCount = new AtomicInteger();
    private final AtomicInteger escapeCount = new AtomicInteger();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule().silent();

    @Mock
    private Connection conn;
    @Mock
    private DriverDelegate delegate;
    @Mock
    private Key key1;
    @Mock
    private Key key2;
    @Mock
    private Trigger trigger1;
    @Mock
    private Trigger trigger2;
    @Mock
    private JobDetail job1;
    @Mock
    private JobDetail job2;

    @Captor
    private ArgumentCaptor<List<Key>> misfired;

    @Before
    public void setUp() throws Exception {
        when(key1.getGroup()).thenReturn("key1group");
        when(key1.getName()).thenReturn("key1name");
        when(key2.getGroup()).thenReturn("key2group");
        when(key2.getName()).thenReturn("key2name");
        when(trigger1.getJobGroup()).thenReturn("job1group");
        when(trigger1.getJobName()).thenReturn("job1name");
        when(trigger2.getJobGroup()).thenReturn("job2group");
        when(trigger2.getJobName()).thenReturn("job2name");

        when(delegate.selectTriggersForRecoveringJobs(conn)).thenReturn(new Trigger[]{trigger1, trigger2});
        when(delegate.selectJobDetail(eq(conn), eq("job1name"), eq("job1group"), any()))
                .thenAnswer(new Answer<JobDetail>() {
                    @Override
                    public JobDetail answer(InvocationOnMock invocation) throws Throwable {
                        throwCount.incrementAndGet();
                        throw new ClassNotFoundException("He's DEAD, Jim!");
                    }
                });
        when(delegate.selectJobDetail(eq(conn), eq("job2name"), eq("job2group"), any()))
                .thenReturn(job2);
    }

    @Test
    public void testStoreTriggerThrowsExceptionOutsideOfRecovery() throws JobPersistenceException {
        final Fixture fixture = new Fixture();
        try {
            fixture.storeTrigger(conn, new SchedulingContext(), trigger1, null, true, STATE_MISFIRED, true, true);
            fail("Should have thrown JobPersistenceException");
        } catch (JobPersistenceException jpe) {
            assertThat(jpe.getMessage(), containsString("He's DEAD, Jim!"));
            assertEquals("callCount", 1, callCount.get());
            assertEquals("throwCount", 1, throwCount.get());
            assertEquals("escapeCount", 1, escapeCount.get());
        }
    }

    @Test
    public void testStoreTriggerDoesNotThrowExceptionFromRecovery() throws Exception {
        when(delegate.selectMisfiredTriggersInStates(
                eq(conn), eq(STATE_MISFIRED), eq(STATE_WAITING), anyLong(),
                anyInt(), misfired.capture())).thenAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                final List<Key> keys = misfired.getValue();
                keys.add(key1);
                keys.add(key2);
                return true;
            }
        });

        final Fixture fixture = new Fixture();
        fixture.recoverJobs();
        assertEquals("callCount", 2, callCount.get());
        assertEquals("throwCount", 1, throwCount.get());
        assertEquals("escapeCount", 0, escapeCount.get());
    }


    class Fixture extends Quartz1HardenedJobStore {

        @Override
        protected DriverDelegate getDelegate() throws NoSuchDelegateException {
            return delegate;
        }

        @Override
        protected Object executeInLock(String lockName, TransactionCallback txCallback) throws JobPersistenceException {
            return txCallback.execute(conn);
        }

        @Override
        protected void executeInNonManagedTXLock(
                final String lockName,
                final VoidTransactionCallback txCallback) throws JobPersistenceException {
            txCallback.execute(conn);
        }

        @Override
        protected Logger getLog() {
            return LOG;
        }

        @Override
        protected boolean jobExists(Connection conn, String jobName, String groupName) throws JobPersistenceException {
            return true;
        }

        @Override
        protected void storeTrigger(Connection conn, SchedulingContext ctxt, Trigger newTrigger,
                                    @Nullable JobDetail job, boolean replaceExisting, String state, boolean forceState,
                                    boolean recovering) throws JobPersistenceException {
            callCount.incrementAndGet();
            boolean ok = false;
            try {
                super.storeTrigger(conn, ctxt, newTrigger, job, replaceExisting, state, forceState, recovering);
                ok = true;
            } finally {
                if (!ok) {
                    escapeCount.incrementAndGet();
                }
            }
        }
    }
}