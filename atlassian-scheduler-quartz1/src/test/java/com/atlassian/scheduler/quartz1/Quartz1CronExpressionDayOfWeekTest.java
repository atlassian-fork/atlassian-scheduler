package com.atlassian.scheduler.quartz1;

import com.atlassian.scheduler.core.tests.CronExpressionDayOfWeekTest;

/**
 * @since v1.5
 */
public class Quartz1CronExpressionDayOfWeekTest extends CronExpressionDayOfWeekTest {
    public Quartz1CronExpressionDayOfWeekTest() {
        super(new Quartz1CronFactory());
    }
}
