package com.atlassian.scheduler.core.tests;

import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.satisfiedBy;
import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;

/**
 * Tests covering the functionality of the minutes cron field.
 *
 * @since v1.5
 */
public abstract class CronExpressionMinutesTest extends AbstractCronExpressionTest {
    protected CronExpressionMinutesTest(final CronFactory cronFactory) {
        super(cronFactory);
    }

    private void minutesBy17s(final String minutesExpr) {
        assertCron(minutesExpr, "0 " + minutesExpr + " 0 1 1 ?",
                satisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(0, 1, 0, 1, 1, 2014),
                unsatisfiedBy(0, 16, 0, 1, 1, 2014),
                satisfiedBy(0, 17, 0, 1, 1, 2014),
                unsatisfiedBy(0, 20, 0, 1, 1, 2014),
                satisfiedBy(0, 34, 0, 1, 1, 2014),
                satisfiedBy(0, 51, 0, 1, 1, 2014),
                unsatisfiedBy(0, 52, 0, 1, 1, 2014),
                unsatisfiedBy(0, 59, 0, 1, 1, 2014));
    }

    @Test
    public void testMinutesIncrement_simple() {
        minutesBy17s("/17");
        minutesBy17s("*/17");
        minutesBy17s("0/17");
        minutesBy17s("0-52/17");

        assertCron("3/17", "0 3/17 0 1 1 ?",
                satisfiedBy(0, 3, 0, 1, 1, 2014),
                unsatisfiedBy(0, 17, 0, 1, 1, 2014),
                satisfiedBy(0, 20, 0, 1, 1, 2014),
                satisfiedBy(0, 37, 0, 1, 1, 2014),
                unsatisfiedBy(0, 51, 0, 1, 1, 2014),
                satisfiedBy(0, 54, 0, 1, 1, 2014));
    }

    @Test
    public void testMinutesIncrement_normalRange() {
        assertCron("0-50/17", "0 0-50/17 0 1 1 ?",
                satisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(0, 7, 0, 1, 1, 2014),
                satisfiedBy(0, 17, 0, 1, 1, 2014),
                unsatisfiedBy(0, 24, 0, 1, 1, 2014),
                satisfiedBy(0, 34, 0, 1, 1, 2014),
                unsatisfiedBy(0, 50, 0, 1, 1, 2014),
                unsatisfiedBy(0, 51, 0, 1, 1, 2014));

        assertCron("20-50/10", "0 20-50/10 0 1 1 ?",
                unsatisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(0, 10, 0, 1, 1, 2014),
                satisfiedBy(0, 20, 0, 1, 1, 2014),
                satisfiedBy(0, 30, 0, 1, 1, 2014),
                satisfiedBy(0, 40, 0, 1, 1, 2014),
                satisfiedBy(0, 50, 0, 1, 1, 2014));

        assertCron("3-37/17", "0 3-37/17 0 1 1 ?",
                satisfiedBy(0, 3, 0, 1, 1, 2014),
                unsatisfiedBy(0, 17, 0, 1, 1, 2014),
                satisfiedBy(0, 20, 0, 1, 1, 2014),
                unsatisfiedBy(0, 21, 0, 1, 1, 2014),
                satisfiedBy(0, 37, 0, 1, 1, 2014),
                unsatisfiedBy(0, 54, 0, 1, 1, 2014),
                unsatisfiedBy(0, 59, 0, 1, 1, 2014));
    }

    @Test
    public void testMinutesIncrement_wrappedRange() {
        assertCron("50-0/17", "0 50-0/17 0 1 1 ?",
                unsatisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(0, 7, 0, 1, 1, 2014),
                unsatisfiedBy(0, 17, 0, 1, 1, 2014),
                unsatisfiedBy(0, 24, 0, 1, 1, 2014),
                unsatisfiedBy(0, 34, 0, 1, 1, 2014),
                satisfiedBy(0, 50, 0, 1, 1, 2014),
                unsatisfiedBy(0, 51, 0, 1, 1, 2014));

        assertCron("50-20/10", "0 50-20/10 0 1 1 ?",
                satisfiedBy(0, 0, 0, 1, 1, 2014),
                satisfiedBy(0, 10, 0, 1, 1, 2014),
                satisfiedBy(0, 20, 0, 1, 1, 2014),
                unsatisfiedBy(0, 30, 0, 1, 1, 2014),
                unsatisfiedBy(0, 40, 0, 1, 1, 2014),
                satisfiedBy(0, 50, 0, 1, 1, 2014));

        assertCron("37-3/17", "0 37-3/17 0 1 1 ?",
                unsatisfiedBy(0, 3, 0, 1, 1, 2014),
                unsatisfiedBy(0, 11, 0, 1, 1, 2014),
                unsatisfiedBy(0, 17, 0, 1, 1, 2014),
                unsatisfiedBy(0, 20, 0, 1, 1, 2014),
                unsatisfiedBy(0, 21, 0, 1, 1, 2014),
                satisfiedBy(0, 37, 0, 1, 1, 2014),
                satisfiedBy(0, 54, 0, 1, 1, 2014),
                unsatisfiedBy(0, 59, 0, 1, 1, 2014));
    }
}
