package com.atlassian.scheduler.core.tests;

import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.satisfiedBy;
import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;

/**
 * Tests covering the functionality of the day-of-week cron field.
 *
 * @since v1.5
 */
public class CronExpressionDayOfWeekTest extends AbstractCronExpressionTest {
    public CronExpressionDayOfWeekTest(CronFactory cronFactory) {
        super(cronFactory);
    }

    @Test
    public void testLastDayOfWeekIsSaturday() {
        // This is dumb because it just means the same thing as 7 or SAT
        assertCron("L (Saturday)", "0 0 0 ? * L",
                unsatisfiedBy(0, 0, 0, 10, 1, 2014),
                satisfiedBy(0, 0, 0, 11, 1, 2014),
                unsatisfiedBy(0, 0, 0, 12, 1, 2014),
                satisfiedBy(0, 0, 0, 18, 1, 2014),
                unsatisfiedBy(0, 0, 0, 24, 1, 2014),
                satisfiedBy(0, 0, 0, 25, 1, 2014),
                unsatisfiedBy(0, 0, 0, 26, 1, 2014),
                unsatisfiedBy(0, 0, 0, 31, 1, 2014),
                satisfiedBy(0, 0, 0, 1, 2, 2014));
    }

    @Test
    public void testLastDayOfWeekForMonth() {
        // With a digit, the meaning changes to the last occurrence of that day-of-week for the month
        // For example, 5L means the last Thursday of the month.
        assertCron("5L", "0 0 0 ? * 5L",
                unsatisfiedBy(0, 0, 0, 9, 1, 2014),
                unsatisfiedBy(0, 0, 0, 23, 1, 2014),
                unsatisfiedBy(0, 0, 0, 23, 1, 2014),
                unsatisfiedBy(0, 0, 0, 29, 1, 2014),
                satisfiedBy(0, 0, 0, 30, 1, 2014),
                unsatisfiedBy(0, 0, 0, 31, 1, 2014),
                unsatisfiedBy(0, 0, 0, 6, 2, 2014),
                satisfiedBy(0, 0, 0, 27, 2, 2014),
                unsatisfiedBy(0, 0, 0, 28, 2, 2014),
                satisfiedBy(0, 0, 0, 29, 2, 2024));

        assertCron("7L", "0 0 0 ? * 7L",
                unsatisfiedBy(0, 0, 0, 11, 1, 2014),
                satisfiedBy(0, 0, 0, 25, 1, 2014),
                unsatisfiedBy(0, 0, 0, 1, 2, 2014));
    }

    @Test
    public void testEnglishDayOfWeek() {
        assertCron("MON", "0 0 0 ? 2 MON",
                satisfiedBy(0, 0, 0, 3, 2, 2014),
                satisfiedBy(0, 0, 0, 10, 2, 2014),
                satisfiedBy(0, 0, 0, 17, 2, 2014),
                satisfiedBy(0, 0, 0, 24, 2, 2014),
                unsatisfiedBy(1, 0, 0, 3, 2, 2014),
                unsatisfiedBy(0, 1, 0, 3, 2, 2014),
                unsatisfiedBy(0, 0, 1, 3, 2, 2014),
                unsatisfiedBy(0, 0, 0, 4, 2, 2014),
                unsatisfiedBy(0, 0, 0, 3, 3, 2014),
                satisfiedBy(0, 0, 0, 2, 2, 2015));
    }

    @Test
    public void testEnglishDayOfWeekNormalRange() {
        assertCron("MON-THU - Other field mismatches", "1 1 1 ? * MON-THU",
                unsatisfiedBy(0, 1, 1, 3, 2, 2014),
                unsatisfiedBy(2, 1, 1, 3, 2, 2014),
                unsatisfiedBy(1, 0, 1, 3, 2, 2014),
                unsatisfiedBy(1, 2, 1, 3, 2, 2014),
                unsatisfiedBy(1, 1, 0, 3, 2, 2014),
                unsatisfiedBy(1, 1, 2, 3, 2, 2014));

        assertCron("Checking MON-THU - Simple checks", "1 1 1 ? * MON-THU",
                unsatisfiedBy(1, 1, 1, 27, 4, 2014),  // SUN
                satisfiedBy(1, 1, 1, 28, 4, 2014),  // MON
                satisfiedBy(1, 1, 1, 29, 4, 2014),  // TUE
                satisfiedBy(1, 1, 1, 30, 4, 2014),  // WED
                satisfiedBy(1, 1, 1, 1, 5, 2014),  // THU
                unsatisfiedBy(1, 1, 1, 2, 5, 2014),  // FRI
                unsatisfiedBy(1, 1, 1, 3, 5, 2014),  // SAT
                unsatisfiedBy(1, 1, 1, 4, 5, 2014),  // SUN
                satisfiedBy(1, 1, 1, 5, 5, 2014),  // MON
                satisfiedBy(1, 1, 1, 6, 5, 2014));  // TUE
    }

    @Test
    public void testEnglishDayOfWeekWrappedRange() {
        assertCron("Checking THU-MON - Other field mismatches", "1 1 1 ? * THU-MON",
                unsatisfiedBy(0, 1, 1, 3, 2, 2014),
                unsatisfiedBy(2, 1, 1, 3, 2, 2014),
                unsatisfiedBy(1, 0, 1, 3, 2, 2014),
                unsatisfiedBy(1, 2, 1, 3, 2, 2014),
                unsatisfiedBy(1, 1, 0, 3, 2, 2014),
                unsatisfiedBy(1, 1, 2, 3, 2, 2014));

        assertCron("Checking THU-MON - Simple checks", "1 1 1 ? * THU-MON",
                satisfiedBy(1, 1, 1, 27, 4, 2014),  // SUN
                satisfiedBy(1, 1, 1, 28, 4, 2014),  // MON
                unsatisfiedBy(1, 1, 1, 29, 4, 2014),  // TUE
                unsatisfiedBy(1, 1, 1, 30, 4, 2014),  // WED
                satisfiedBy(1, 1, 1, 1, 5, 2014),  // THU
                satisfiedBy(1, 1, 1, 2, 5, 2014),  // FRI
                satisfiedBy(1, 1, 1, 3, 5, 2014),  // SAT
                satisfiedBy(1, 1, 1, 4, 5, 2014),  // SUN
                satisfiedBy(1, 1, 1, 5, 5, 2014),  // MON
                unsatisfiedBy(1, 1, 1, 6, 5, 2014));  // TUE

    }

    @Test
    public void testThirdMondayOfMonthUsingName() {
        nthDay3rdMonday("5 6 7 ? 1 MON#3");
    }

    @Test
    public void testThirdMondayOfMonthUsingNumber() {
        nthDay3rdMonday("5 6 7 ? 1 2#3");
    }

    private void nthDay3rdMonday(final String cronExpression) {
        assertCron("3rd Monday - Other field mismatches", cronExpression,
                unsatisfiedBy(4, 6, 7, 20, 1, 2014),
                unsatisfiedBy(5, 5, 7, 20, 1, 2014),
                unsatisfiedBy(5, 6, 6, 20, 1, 2014),
                unsatisfiedBy(5, 6, 7, 20, 2, 2014),
                unsatisfiedBy(5, 6, 7, 20, 1, 2015));

        assertCron("3rd Monday - simple checks", cronExpression,
                satisfiedBy(5, 6, 7, 21, 1, 2013),
                unsatisfiedBy(5, 6, 7, 13, 1, 2014),
                unsatisfiedBy(5, 6, 7, 19, 1, 2014),
                satisfiedBy(5, 6, 7, 20, 1, 2014),
                unsatisfiedBy(5, 6, 7, 21, 1, 2014),
                unsatisfiedBy(5, 6, 7, 27, 1, 2014),
                unsatisfiedBy(5, 6, 7, 17, 2, 2014),
                satisfiedBy(5, 6, 7, 20, 1, 2014),
                satisfiedBy(5, 6, 7, 18, 1, 2016),
                satisfiedBy(5, 6, 7, 16, 1, 2017));
    }

    @Test
    public void testNthDayOfMonth() {
        assertCron("TUE#4 (4th Tuesday)", "9 9 9 ? * TUE#4",
                unsatisfiedBy(9, 9, 9, 21, 1, 2014),
                unsatisfiedBy(9, 9, 9, 27, 1, 2014),
                satisfiedBy(9, 9, 9, 28, 1, 2014),
                unsatisfiedBy(9, 9, 9, 29, 1, 2014),
                unsatisfiedBy(9, 9, 9, 4, 2, 2014),
                satisfiedBy(9, 9, 9, 25, 2, 2014),
                satisfiedBy(9, 9, 9, 22, 4, 2014),
                satisfiedBy(9, 9, 9, 24, 1, 2017),
                satisfiedBy(9, 9, 9, 28, 3, 2017));

        assertCron("4#5 (5th Wednesday)", "9 9 9 ? * 4#5",
                satisfiedBy(9, 9, 9, 29, 2, 2012),
                unsatisfiedBy(9, 9, 9, 28, 1, 2014),
                unsatisfiedBy(9, 9, 9, 4, 2, 2014),
                unsatisfiedBy(9, 9, 9, 31, 3, 2014),
                satisfiedBy(9, 9, 9, 30, 4, 2014),
                satisfiedBy(9, 9, 9, 31, 12, 2014),
                satisfiedBy(9, 9, 9, 29, 3, 2017));
    }
}
