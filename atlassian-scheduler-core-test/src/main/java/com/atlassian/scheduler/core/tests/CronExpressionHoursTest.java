package com.atlassian.scheduler.core.tests;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.runTime;
import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.satisfiedBy;
import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;
import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * Tests covering the functionality of the hours cron field.
 *
 * @since v1.5
 */
public abstract class CronExpressionHoursTest extends AbstractCronExpressionTest {
    protected CronExpressionHoursTest(CronFactory cronFactory) {
        super(cronFactory);
    }

    private void cronIncrements_hours_by7s(final String hoursExpr) {
        assertCron(hoursExpr, "0 0 " + hoursExpr + " 1 1 ?",
                satisfiedBy(0, 0, 0, 1, 1, 2014),
                unsatisfiedBy(0, 0, 1, 1, 1, 2014),
                unsatisfiedBy(0, 0, 6, 1, 1, 2014),
                satisfiedBy(0, 0, 7, 1, 1, 2014),
                satisfiedBy(0, 0, 14, 1, 1, 2014),
                satisfiedBy(0, 0, 21, 1, 1, 2014),
                unsatisfiedBy(0, 0, 23, 1, 1, 2014));
    }

    @Test
    public void testHoursIncrement() {
        cronIncrements_hours_by7s("/7");
        cronIncrements_hours_by7s("*/7");
        cronIncrements_hours_by7s("0/7");
        cronIncrements_hours_by7s("0-22/7");

        assertCron("2/5", "0 0 2/5 1 1 ?",
                satisfiedBy(0, 0, 2, 1, 1, 2014),
                unsatisfiedBy(0, 0, 6, 1, 1, 2014),
                satisfiedBy(0, 0, 7, 1, 1, 2014),
                unsatisfiedBy(0, 0, 8, 1, 1, 2014),
                satisfiedBy(0, 0, 12, 1, 1, 2014),
                satisfiedBy(0, 0, 17, 1, 1, 2014),
                satisfiedBy(0, 0, 22, 1, 1, 2014),
                unsatisfiedBy(0, 0, 23, 1, 1, 2014));
    }

    @Test
    public void testHoursIncrementNormalRange() {
        assertCron("9-17/3", "0 0 9-17/3 1 1 ?",
                unsatisfiedBy(0, 0, 2, 1, 1, 2014),
                unsatisfiedBy(0, 0, 5, 1, 1, 2014),
                unsatisfiedBy(0, 0, 6, 1, 1, 2014),
                unsatisfiedBy(0, 0, 8, 1, 1, 2014),
                satisfiedBy(0, 0, 9, 1, 1, 2014),
                unsatisfiedBy(0, 0, 11, 1, 1, 2014),
                satisfiedBy(0, 0, 12, 1, 1, 2014),
                unsatisfiedBy(0, 0, 13, 1, 1, 2014),
                unsatisfiedBy(0, 0, 14, 1, 1, 2014),
                satisfiedBy(0, 0, 15, 1, 1, 2014),
                unsatisfiedBy(0, 0, 16, 1, 1, 2014),
                unsatisfiedBy(0, 0, 17, 1, 1, 2014),
                unsatisfiedBy(0, 0, 18, 1, 1, 2014),
                unsatisfiedBy(0, 0, 19, 1, 1, 2014),
                unsatisfiedBy(0, 0, 20, 1, 1, 2014),
                unsatisfiedBy(0, 0, 21, 1, 1, 2014),
                unsatisfiedBy(0, 0, 23, 1, 1, 2014));
    }

    @Test
    public void testHoursIncrementWrappedRange() {
        assertCron("17-9/3", "0 0 17-9/3 1 1 ?",
                satisfiedBy(0, 0, 2, 1, 1, 2014),
                satisfiedBy(0, 0, 5, 1, 1, 2014),
                unsatisfiedBy(0, 0, 6, 1, 1, 2014),
                satisfiedBy(0, 0, 8, 1, 1, 2014),
                unsatisfiedBy(0, 0, 9, 1, 1, 2014),
                unsatisfiedBy(0, 0, 11, 1, 1, 2014),
                unsatisfiedBy(0, 0, 12, 1, 1, 2014),
                unsatisfiedBy(0, 0, 13, 1, 1, 2014),
                unsatisfiedBy(0, 0, 14, 1, 1, 2014),
                unsatisfiedBy(0, 0, 15, 1, 1, 2014),
                unsatisfiedBy(0, 0, 16, 1, 1, 2014),
                satisfiedBy(0, 0, 17, 1, 1, 2014),
                unsatisfiedBy(0, 0, 18, 1, 1, 2014),
                unsatisfiedBy(0, 0, 19, 1, 1, 2014),
                satisfiedBy(0, 0, 20, 1, 1, 2014),
                unsatisfiedBy(0, 0, 21, 1, 1, 2014),
                satisfiedBy(0, 0, 23, 1, 1, 2014));
    }

    @Test
    public void testHoursDaylightSavingsGap() {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Sydney");
        final DateTime startingAt = new DateTime(2014, 10, 4, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        assertRunTimes("Australia/Sydney; 2014/10/05; 02 => 03 (On the hour)", "0 0 * * * ?", startingAt,
                runTime(0, 0, 0, 5, 10, 2014, zone, base + MINUTES.toMillis(30)),
                runTime(0, 0, 1, 5, 10, 2014, zone, base + MINUTES.toMillis(90)),
                runTime(0, 0, 3, 5, 10, 2014, zone, base + MINUTES.toMillis(150)),
                runTime(0, 0, 4, 5, 10, 2014, zone, base + MINUTES.toMillis(210)));

        assertRunTimes("Australia/Sydney; 2014/10/05; 02 => 03 (Within the hour)", "0 30 * * * ?", startingAt,
                runTime(0, 30, 0, 5, 10, 2014, zone, base + MINUTES.toMillis(60)),
                runTime(0, 30, 1, 5, 10, 2014, zone, base + MINUTES.toMillis(120)),
                runTime(0, 30, 3, 5, 10, 2014, zone, base + MINUTES.toMillis(180)),
                runTime(0, 30, 4, 5, 10, 2014, zone, base + MINUTES.toMillis(240)));
    }

    @Test
    public void testHoursDaylightSavingsOverlap() {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Sydney");
        final DateTime startingAt = new DateTime(2014, 4, 5, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        // The first 01:30 runs, the second one is skipped, and we run again at 02:30.  The result is
        // that the runs at 01:30 and 02:30 are 2 hours apart instead of 1 hour.
        assertRunTimes("Australia/Sydney; 2014/04/06", "0 30 * * * ?", startingAt,
                runTime(0, 30, 0, 6, 4, 2014, zone, base + MINUTES.toMillis(60)),
                runTime(0, 30, 1, 6, 4, 2014, zone, base + MINUTES.toMillis(120)),
                runTime(0, 30, 2, 6, 4, 2014, zone, base + MINUTES.toMillis(240)),
                runTime(0, 30, 3, 6, 4, 2014, zone, base + MINUTES.toMillis(300)),
                runTime(0, 30, 4, 6, 4, 2014, zone, base + MINUTES.toMillis(360)));
    }

    /**
     * This is probably not the behaviour that we really want, but unfortunately it's how the cron schedules are
     * defined to work.  You may think you have scheduled something that will run once a minute, but it will not
     * run during the DST overlap.
     */
    @Test
    public void testHoursDaylightSavingsOverlapEveryMinute() {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Sydney");
        final DateTime startingAt = new DateTime(2014, 4, 6, 1, 57, 0, zone);
        final long base = startingAt.getMillis();

        // So it means once a minute, but the DST overlap causes us to skip a whole hour of it. :(
        assertRunTimes("Australia/Sydney; 2014/04/06", "0 * * * * ?", startingAt,
                runTime(0, 58, 1, 6, 4, 2014, zone, base + MINUTES.toMillis(1)),
                runTime(0, 59, 1, 6, 4, 2014, zone, base + MINUTES.toMillis(2)),
                runTime(0, 0, 2, 6, 4, 2014, zone, base + MINUTES.toMillis(63)),
                runTime(0, 1, 2, 6, 4, 2014, zone, base + MINUTES.toMillis(64)));
    }

    /**
     * Lord Howe Island, Australia just had to be different.
     * <p>
     * Its daylight savings transition is 30 minutes instead of a full hour.  Welcome to crazy town!
     * </p>
     */
    @Test
    public void testHoursDaylightSavingsGapLordHowe() {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Lord_Howe");
        final DateTime startingAt = new DateTime(2014, 10, 4, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        assertRunTimes("Australia/Lord_Howe; 2014/10/05", "0 0 * * * ?", startingAt,
                runTime(0, 0, 0, 5, 10, 2014, zone, base + MINUTES.toMillis(30)),
                runTime(0, 0, 1, 5, 10, 2014, zone, base + MINUTES.toMillis(90)),
                runTime(0, 0, 3, 5, 10, 2014, zone, base + MINUTES.toMillis(180)),
                runTime(0, 0, 4, 5, 10, 2014, zone, base + MINUTES.toMillis(240)));
    }

    @Test
    public void testHoursDaylightSavingsOverlapLordHowe() {

        final DateTimeZone zone = DateTimeZone.forID("Australia/Lord_Howe");
        final DateTime startingAt = new DateTime(2014, 4, 5, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        // 00:30 and 01:30 are *90 minutes* apart.
        assertRunTimes("Australia/Lord_Howe; 2014/04/06", "0 30 * * * ?", startingAt,
                runTime(0, 30, 0, 6, 4, 2014, zone, base + MINUTES.toMillis(60)),
                runTime(0, 30, 1, 6, 4, 2014, zone, base + MINUTES.toMillis(150)),
                runTime(0, 30, 2, 6, 4, 2014, zone, base + MINUTES.toMillis(210)),
                runTime(0, 30, 3, 6, 4, 2014, zone, base + MINUTES.toMillis(270)),
                runTime(0, 30, 4, 6, 4, 2014, zone, base + MINUTES.toMillis(330)));
    }

}
