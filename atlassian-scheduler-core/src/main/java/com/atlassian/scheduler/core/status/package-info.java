/**
 * Core implementations for a job's details and run history.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.core.status;

import javax.annotation.ParametersAreNonnullByDefault;