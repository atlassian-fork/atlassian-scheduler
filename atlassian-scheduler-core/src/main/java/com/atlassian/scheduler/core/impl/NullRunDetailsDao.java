package com.atlassian.scheduler.core.impl;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import com.atlassian.scheduler.status.RunDetails;

/**
 * An implementation of {@code RunDetailsDao} that silently discards all run details supplied to it.
 *
 * @since v1.0
 */
public class NullRunDetailsDao implements RunDetailsDao {
    @Override
    public RunDetails getLastRunForJob(JobId jobId) {
        return null;
    }

    @Override
    public RunDetails getLastSuccessfulRunForJob(JobId jobId) {
        return null;
    }

    @Override
    public void addRunDetails(JobId jobId, RunDetails runDetails) {
    }
}
