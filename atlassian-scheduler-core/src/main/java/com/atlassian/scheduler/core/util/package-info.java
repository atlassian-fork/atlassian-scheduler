/**
 * Various helpful internal utilities.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.core.util;

import javax.annotation.ParametersAreNonnullByDefault;