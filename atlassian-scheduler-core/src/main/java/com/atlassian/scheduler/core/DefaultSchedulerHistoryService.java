package com.atlassian.scheduler.core;

import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import com.atlassian.scheduler.status.RunDetails;

import javax.annotation.CheckForNull;

/**
 * @since 1.0
 */
public class DefaultSchedulerHistoryService implements SchedulerHistoryService {
    private final RunDetailsDao runDetailsDao;

    public DefaultSchedulerHistoryService(RunDetailsDao runDetailsDao) {
        this.runDetailsDao = runDetailsDao;
    }

    @Override
    @CheckForNull
    public RunDetails getLastSuccessfulRunForJob(JobId jobId) {
        return runDetailsDao.getLastSuccessfulRunForJob(jobId);
    }

    @Override
    @CheckForNull
    public RunDetails getLastRunForJob(JobId jobId) {
        return runDetailsDao.getLastRunForJob(jobId);
    }
}
