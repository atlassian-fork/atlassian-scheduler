package com.atlassian.scheduler.core.impl;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import com.atlassian.scheduler.status.RunDetails;
import com.atlassian.scheduler.status.RunOutcome;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import static java.util.concurrent.TimeUnit.DAYS;

/**
 * An implementation of the {@code RunDetailsDao} that keeps the scheduler history in memory.
 * This implementation does not share information across nodes in the cluster or persist it
 * across restarts, but provides an otherwise functional implementation.  By default, each
 * job's history is expired 30 days after it is last updated.
 *
 * @since v1.0
 */
public class MemoryRunDetailsDao implements RunDetailsDao {
    private final Cache<JobId, JobRecord> store;

    public MemoryRunDetailsDao() {
        this(30);
    }

    public MemoryRunDetailsDao(int daysToKeepIdleHistory) {
        this.store = CacheBuilder.newBuilder()
                .expireAfterWrite(daysToKeepIdleHistory, DAYS)
                .<JobId, JobRecord>build();
    }


    @Override
    public RunDetails getLastRunForJob(JobId jobId) {
        final JobRecord record = store.getIfPresent(jobId);
        return (record != null) ? record.lastRun : null;
    }

    @Override
    public RunDetails getLastSuccessfulRunForJob(JobId jobId) {
        final JobRecord record = store.getIfPresent(jobId);
        return (record != null) ? record.lastRun : null;
    }

    @Override
    public void addRunDetails(JobId jobId, RunDetails runDetails) {
        // Concurrency note: while we won't get concurrent modification exceptions or anything like
        // that, little effort is made here to keep this information consistent if multiple instances
        // of the same JobId attempt to record run details at once.  This is not supposed to happen,
        // anyway, so it should not be important.
        final JobRecord record;
        if (runDetails.getRunOutcome() == RunOutcome.SUCCESS) {
            record = new JobRecord(runDetails, runDetails);
        } else {
            record = new JobRecord(runDetails, getLastSuccessfulRunForJob(jobId));
        }
        store.put(jobId, record);
    }


    static class JobRecord {
        final RunDetails lastRun;
        final RunDetails lastSuccessfulRun;

        JobRecord(RunDetails lastRun, RunDetails lastSuccessfulRun) {
            this.lastRun = lastRun;
            this.lastSuccessfulRun = lastSuccessfulRun;
        }
    }
}
