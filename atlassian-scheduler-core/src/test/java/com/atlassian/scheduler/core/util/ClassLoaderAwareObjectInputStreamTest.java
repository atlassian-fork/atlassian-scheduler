package com.atlassian.scheduler.core.util;

import org.junit.Test;

import java.io.IOException;
import java.io.ObjectStreamClass;
import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

import static com.atlassian.scheduler.core.Constants.BYTES_DEADF00D;
import static com.atlassian.scheduler.core.Constants.BYTES_EMPTY_MAP;
import static com.atlassian.scheduler.core.Constants.BYTES_NULL;
import static com.atlassian.scheduler.core.Constants.BYTES_PARAMETERS;
import static com.atlassian.scheduler.core.Constants.EMPTY_MAP;
import static com.atlassian.scheduler.core.Constants.PARAMETERS;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @since v1.0
 */
@SuppressWarnings("ConstantConditions")
public class ClassLoaderAwareObjectInputStreamTest {

    @Test
    public void testNulls() {
        assertConstructorThrows(NullPointerException.class, null, null);
        assertConstructorThrows(NullPointerException.class, null, BYTES_PARAMETERS);
        assertConstructorThrows(NullPointerException.class, getClass().getClassLoader(), null);
    }

    @Test
    public void testInvalidSerializationData() {
        assertConstructorThrows(IOException.class, getClass().getClassLoader(), BYTES_DEADF00D);
    }

    @Test
    public void testNullMap() throws IOException, ClassNotFoundException {
        assertReadObject(null, getClass().getClassLoader(), BYTES_NULL);
    }

    @Test
    public void testEmptyMap() throws IOException, ClassNotFoundException {
        assertReadObject(EMPTY_MAP, getClass().getClassLoader(), BYTES_EMPTY_MAP);
    }

    @Test
    public void testParameters() throws IOException, ClassNotFoundException {
        assertReadObject(PARAMETERS, getClass().getClassLoader(), BYTES_PARAMETERS);
    }

    @Test
    public void testResolveClassYieldsFirstExceptionWhenNotFound() throws Exception {
        // The system classloader isn't going to find "foo.bar" either, and *our* exception is the one we want
        final ClassNotFoundException cnfe = new ClassNotFoundException("Expected");
        assertResolveClassThrows(cnfe, classLoaderThatThrows(cnfe), "foo.bar");
    }

    @Test
    public void testResolveClassSucceedsUsingOurClassLoader() throws Exception {
        MockClassLoader classLoader = new MockClassLoader(String.class, String.class.getName());

        assertResolveClass(String.class, classLoader, String.class.getName());

        // Make sure it came from us, since the system classloader would have succeeded too
        assertTrue(classLoader.loaded());
    }

    @Test
    public void testResolveClassSucceedsByFallbackForNormalClasses() throws Exception {
        // The system classloader with find java.lang.String
        final ClassNotFoundException cnfe = new ClassNotFoundException("Expected");
        assertResolveClass(String.class, classLoaderThatThrows(cnfe), String.class.getName());
    }

    @Test
    public void testResolveClassSucceedsByFallbackForPrimitives() throws Exception {
        // A special case that ObjectInputStream supports...
        final ClassNotFoundException cnfe = new ClassNotFoundException("Expected");
        assertResolveClass(Integer.TYPE, classLoaderThatThrows(cnfe), "int");
    }

    @Test
    public void testResolveClassDoesNotAttemptFallbackOnSerializationErrors() throws Exception {
        // The system classloader would find java.lang.String, but since we throw IOException it does not try
        final RuntimeException exception = new RuntimeException("Expected");
        assertResolveClassThrows(exception, classLoaderThatThrows(exception), String.class.getName());
    }

    static ClassLoader classLoaderThatThrows(ClassNotFoundException e) {
        return new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                throw e;
            }
        };
    }

    static ClassLoader classLoaderThatThrows(RuntimeException e) {
        return new ClassLoader() {
            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                throw e;
            }
        };
    }

    static ObjectStreamClass desc(String className) {
        final ObjectStreamClass osc = mock(ObjectStreamClass.class);
        when(osc.getName()).thenReturn(className);
        return osc;
    }

    static void assertConstructorThrows(final Class<? extends Exception> expected,
                                        final ClassLoader classLoader, final byte[] bytes) {
        try {
            final ClassLoaderAwareObjectInputStream is = new ClassLoaderAwareObjectInputStream(classLoader, bytes);
            is.close();
            try {
                fail("Expected construction to fail with " + expected.getName() + ", but it succeeded!");
            } finally {
                is.close();
            }
        } catch (Exception ex) {
            assertThat(ex, instanceOf(expected));
        }
    }

    private static void assertReadObject(final Map<String, Serializable> expected,
                                         final ClassLoader classLoader, final byte[] bytes) throws IOException, ClassNotFoundException {
        final ClassLoaderAwareObjectInputStream is = new ClassLoaderAwareObjectInputStream(classLoader, bytes);
        try {
            assertEquals(expected, is.readObject());
        } finally {
            is.close();
        }
    }

    private static void assertResolveClass(final Class<?> expected,
                                           final ClassLoader classLoader, final String className) throws IOException, ClassNotFoundException {
        final ClassLoaderAwareObjectInputStream is = new ClassLoaderAwareObjectInputStream(classLoader, BYTES_EMPTY_MAP);
        try {
            assertEquals(expected, is.resolveClass(desc(className)));
        } finally {
            is.close();
        }
    }

    private static void assertResolveClassThrows(final Exception expected,
                                                 final ClassLoader classLoader, final String className) throws IOException, ClassNotFoundException {
        final ClassLoaderAwareObjectInputStream is = new ClassLoaderAwareObjectInputStream(classLoader, BYTES_EMPTY_MAP);
        try {
            throw new AssertionError("Expected resolveClass to throw " + expected + ", but got " +
                    is.resolveClass(desc(className)).getName());
        } catch (Exception ex) {
            assertSame(expected, ex);
        } finally {
            is.close();
        }
    }

    private static class MockClassLoader extends ClassLoader {

        private final Class c;
        private final String expectedName;

        private boolean loaded;

        public MockClassLoader(Class c, String expectedName) {
            this.c = c;
            this.expectedName = expectedName;
        }

        @Override
        public Class<?> loadClass(String name) throws ClassNotFoundException {
            if (Objects.equals(expectedName, name)) {
                loaded = true;
                return c;
            }
            throw new ClassNotFoundException();
        }

        public boolean loaded() {
            return loaded;
        }
    }
}

