package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.CronScheduleInfo;
import com.atlassian.scheduler.config.IntervalScheduleInfo;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.spi.SchedulerServiceConfiguration;
import com.atlassian.scheduler.core.util.ParameterMapSerializer;
import com.atlassian.scheduler.cron.CronSyntaxException;
import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;

import javax.annotation.Nonnull;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static com.atlassian.scheduler.core.util.CronExpressionQuantizer.quantizeSecondsField;
import static com.atlassian.scheduler.core.util.QuartzParseExceptionMapper.mapException;
import static com.atlassian.scheduler.core.util.TimeIntervalQuantizer.quantizeToMinutes;
import static com.atlassian.scheduler.quartz2.Quartz2SchedulerFacade.QUARTZ_PARAMETERS_KEY;
import static com.atlassian.scheduler.quartz2.Quartz2SchedulerFacade.QUARTZ_TRIGGER_GROUP;
import static org.quartz.CronScheduleBuilder.cronScheduleNonvalidatedExpression;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerKey.triggerKey;

/**
 * @since v1.0
 */
class Quartz2TriggerFactory {
    private final SchedulerServiceConfiguration config;
    private final ParameterMapSerializer parameterMapSerializer;

    Quartz2TriggerFactory(SchedulerServiceConfiguration config, ParameterMapSerializer parameterMapSerializer) {
        this.config = config;
        this.parameterMapSerializer = parameterMapSerializer;
    }

    public TriggerBuilder<?> buildTrigger(final JobId jobId, final JobConfig jobConfig)
            throws SchedulerServiceException {
        final byte[] parameters = parameterMapSerializer.serializeParameters(jobConfig.getParameters());
        final JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put(QUARTZ_PARAMETERS_KEY, parameters);
        return buildTrigger(jobConfig.getSchedule())
                .withIdentity(triggerKey(jobId.toString(), QUARTZ_TRIGGER_GROUP))
                .usingJobData(jobDataMap);
    }

    public TriggerBuilder<?> buildTrigger(Schedule schedule) throws SchedulerServiceException {
        switch (schedule.getType()) {
            case INTERVAL:
                return getSimpleTrigger(schedule.getIntervalScheduleInfo());
            case CRON_EXPRESSION:
                return getCronTrigger(schedule.getCronScheduleInfo());
        }
        throw new IllegalStateException("type=" + schedule.getType());
    }

    private static TriggerBuilder<SimpleTrigger> getSimpleTrigger(IntervalScheduleInfo info) {
        final Date startTime = (info.getFirstRunTime() != null) ? info.getFirstRunTime() : new Date();
        return TriggerBuilder.newTrigger()
                .withSchedule(interval(info.getIntervalInMillis()))
                .startAt(startTime);
    }

    private static SimpleScheduleBuilder interval(final long intervalInMillis) {
        if (intervalInMillis == 0L) {
            return simpleSchedule().withRepeatCount(0);
        }

        return simpleSchedule()
                .withIntervalInMilliseconds(quantizeToMinutes(intervalInMillis))
                .repeatForever();
    }

    private TriggerBuilder<CronTrigger> getCronTrigger(CronScheduleInfo info) throws CronSyntaxException {
        try {
            // Force validation to happen first
            final String cronExpression = new CronExpression(info.getCronExpression()).getCronExpression();
            final CronScheduleBuilder schedule = cronScheduleNonvalidatedExpression(quantizeSecondsField(cronExpression))
                    .inTimeZone(getTimeZone(info));
            return TriggerBuilder.newTrigger()
                    .withSchedule(schedule);
        } catch (ParseException pe) {
            throw mapException(info.getCronExpression().toUpperCase(Locale.US), pe);
        }
    }

    @Nonnull
    private TimeZone getTimeZone(CronScheduleInfo info) {
        TimeZone timeZone = info.getTimeZone();
        if (timeZone == null) {
            timeZone = config.getDefaultTimeZone();
            if (timeZone == null) {
                timeZone = TimeZone.getDefault();
            }
        }
        return timeZone;
    }
}
