package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.SchedulerRuntimeException;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import com.atlassian.scheduler.quartz2.spi.Quartz2SchedulerConfiguration;
import io.atlassian.util.concurrent.LazyReference;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.function.Supplier;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static java.util.Objects.requireNonNull;
import static org.quartz.JobKey.jobKey;
import static org.quartz.TriggerKey.triggerKey;
import static org.quartz.impl.matchers.GroupMatcher.jobGroupEquals;

/**
 * Wraps the quartz scheduler to make it work a bit closer to how we need it to.
 * Specifically, this hides Quartz's checked exceptions and provides the association
 * between our jobId/jobRunnerKey and the trigger and group names.
 */
class Quartz2SchedulerFacade {
    private static final Logger LOG = LoggerFactory.getLogger(Quartz2SchedulerFacade.class);

    static final String QUARTZ_JOB_GROUP = "SchedulerServiceJobs";
    static final String QUARTZ_TRIGGER_GROUP = "SchedulerServiceTriggers";
    static final String QUARTZ_PARAMETERS_KEY = "parameters";

    private final Supplier<Scheduler> quartzRef;

    private Quartz2SchedulerFacade(final Supplier<Scheduler> quartzRef) {
        this.quartzRef = quartzRef;
    }


    /**
     * Creates the local scheduler facade by wrapping a supplied Scheduler instance.
     */
    static Quartz2SchedulerFacade createLocal(final AbstractSchedulerService schedulerService,
                                              final Scheduler scheduler) throws SchedulerServiceException {
        requireNonNull(scheduler, "scheduler");
        return createFacade(schedulerService, scheduler, RUN_LOCALLY);
    }

    /**
     * Creates the clustered scheduler facade by wrapping a supplied Scheduler instance.
     */
    static Quartz2SchedulerFacade createClustered(final AbstractSchedulerService schedulerService,
                                                  final Scheduler scheduler) throws SchedulerServiceException {
        requireNonNull(scheduler, "scheduler");
        return createFacade(schedulerService, scheduler, RUN_ONCE_PER_CLUSTER);
    }

    /**
     * Creates a schedule scheduler facade by wrapping a supplied scheduler instance.
     */
    private static Quartz2SchedulerFacade createFacade(final AbstractSchedulerService schedulerService,
                                                       final Scheduler scheduler,
                                                       final RunMode runMode) throws SchedulerServiceException {
        try {
            configureScheduler(scheduler, schedulerService, runMode);
            final Supplier<Scheduler> quartzRef = () -> scheduler;
            return new Quartz2SchedulerFacade(quartzRef);
        } catch (SchedulerException se) {
            throw checked("Unable to configure the underlying Quartz scheduler", se);
        }
    }


    /**
     * Creates the local scheduler facade lazily using the supplied configuration.
     */
    static Quartz2SchedulerFacade createLocal(final AbstractSchedulerService schedulerService,
                                              final Quartz2SchedulerConfiguration config) throws SchedulerServiceException {
        requireNonNull(config, "config");
        final Properties localSettings = requireNonNull(config.getLocalSettings(), "config.getLocalSettings()");
        return createFacade(schedulerService, localSettings, RUN_LOCALLY);
    }

    /**
     * Creates the clustered scheduler facade lazily using the supplied configuration.
     */
    static Quartz2SchedulerFacade createClustered(final AbstractSchedulerService schedulerService,
                                                  final Quartz2SchedulerConfiguration config) throws SchedulerServiceException {
        requireNonNull(config, "config");
        final Properties clusteredSettings = requireNonNull(config.getClusteredSettings(), "config.getClusteredSettings()");
        return createFacade(schedulerService, clusteredSettings, RUN_ONCE_PER_CLUSTER);
    }

    /**
     * Creates a schedule scheduler facade lazily using the supplied configuration.
     */
    private static Quartz2SchedulerFacade createFacade(final AbstractSchedulerService schedulerService,
                                                       final Properties customConfig, final RunMode runMode)
            throws SchedulerServiceException {
        try {
            final Properties config = new Properties();
            for (String key : customConfig.stringPropertyNames()) {
                config.setProperty(key, customConfig.getProperty(key));
            }

            // JRA-23747 -- never allow Quartz to run its update check
            config.setProperty("org.quartz.scheduler.skipUpdateCheck", "true");

            final StdSchedulerFactory schedulerFactory = new StdSchedulerFactory(config);
            final Supplier<Scheduler> quartzRef = createQuartzRef(schedulerService, runMode, schedulerFactory);
            return new Quartz2SchedulerFacade(quartzRef);
        } catch (SchedulerException se) {
            throw checked("Unable to create the underlying Quartz scheduler", se);
        }
    }


    private static Supplier<Scheduler> createQuartzRef(final AbstractSchedulerService schedulerService,
                                                       final RunMode runMode,
                                                       final StdSchedulerFactory schedulerFactory) {
        return new LazyReference<Scheduler>() {
            @Override
            protected Scheduler create() throws Exception {
                // SCHEDULER-11: Quartz cares about the thread's CCL.  This makes sure that
                // the class loader that was used to construct the Quartz1SchedulerService
                // is set as the thread's CCL at the time the scheduler is lazily constructed.
                // otherwise, if a plugin is given direct access to Quartz scheduler before
                // the application initializes it, the plugin bundle's classloader could be
                // set as the thread's CCL, with unpleasant results like HOT-6239.
                final Thread thd = Thread.currentThread();
                final ClassLoader originalContextClassLoader = thd.getContextClassLoader();
                try {
                    thd.setContextClassLoader(schedulerService.getClass().getClassLoader());
                    final Scheduler quartz = schedulerFactory.getScheduler();
                    configureScheduler(quartz, schedulerService, runMode);
                    return quartz;
                } finally {
                    thd.setContextClassLoader(originalContextClassLoader);
                }
            }
        };
    }

    static void configureScheduler(final Scheduler quartz,
                                   final AbstractSchedulerService schedulerService,
                                   final RunMode runMode) throws SchedulerException {
        quartz.setJobFactory(new Quartz2JobFactory(schedulerService, runMode));
    }


    @Nullable
    Trigger getTrigger(final JobId jobId) {
        try {
            return getScheduler().getTrigger(jobId(jobId));
        } catch (SchedulerException se) {
            logWarn("Error getting quartz trigger for '{}'", jobId, se);
            return null;
        }
    }

    @Nullable
    JobDetail getQuartzJob(final JobRunnerKey jobRunnerKey) {
        try {
            return getScheduler().getJobDetail(jobRunnerKey(jobRunnerKey));
        } catch (SchedulerException se) {
            logWarn("Error getting quartz job details for '{}'", jobRunnerKey, se);
            return null;
        }
    }

    boolean hasAnyTriggers(final JobRunnerKey jobRunnerKey) {
        return !getTriggersOfJob(jobRunnerKey).isEmpty();
    }

    Collection<JobRunnerKey> getJobRunnerKeys() {
        try {
            final Set<JobKey> jobKeys = getScheduler().getJobKeys(jobGroupEquals(QUARTZ_JOB_GROUP));
            final Set<JobRunnerKey> jobRunnerKeys = new HashSet<JobRunnerKey>((int) (jobKeys.size() * 1.25));
            for (JobKey jobKey : jobKeys) {
                jobRunnerKeys.add(JobRunnerKey.of(jobKey.getName()));
            }
            return jobRunnerKeys;
        } catch (SchedulerException se) {
            throw unchecked("Could not get the triggers from Quartz", se);
        }
    }

    List<? extends Trigger> getTriggersOfJob(final JobRunnerKey jobRunnerKey) {
        try {
            return getScheduler().getTriggersOfJob(jobRunnerKey(jobRunnerKey));
        } catch (SchedulerException se) {
            throw unchecked("Could not get the triggers from Quartz", se);
        }
    }

    boolean deleteTrigger(final JobId jobId) {
        try {
            return getScheduler().unscheduleJob(jobId(jobId));
        } catch (SchedulerException se) {
            logWarn("Error removing Quartz trigger for '{}'", jobId, se);
            return false;
        }
    }

    boolean deleteJob(final JobRunnerKey jobRunnerKey) {
        try {
            return getScheduler().deleteJob(jobRunnerKey(jobRunnerKey));
        } catch (SchedulerException se) {
            logWarn("Error removing Quartz job for '{}'", jobRunnerKey, se);
            return false;
        }
    }

    private void scheduleJob(final TriggerBuilder<?> trigger) throws SchedulerServiceException {
        try {
            getScheduler().scheduleJob(trigger.build());
        } catch (SchedulerException se) {
            throw checked("Unable to create the Quartz trigger", se);
        }
    }


    void scheduleJob(final JobRunnerKey jobRunnerKey, final TriggerBuilder<?> trigger) throws SchedulerServiceException {
        if (getQuartzJob(jobRunnerKey) != null) {
            scheduleJob(trigger.forJob(jobRunnerKey(jobRunnerKey)));
            return;
        }

        try {
            final JobDetail quartzJob = JobBuilder.newJob()
                    .withIdentity(jobRunnerKey(jobRunnerKey))
                    .ofType(Quartz2Job.class)
                    .storeDurably(false)
                    .build();
            getScheduler().scheduleJob(quartzJob, trigger.build());
        } catch (SchedulerException se) {
            throw checked("Unable to create the Quartz job and trigger", se);
        }
    }

    boolean unscheduleJob(final JobId jobId) {
        final Trigger trigger = getTrigger(jobId);
        if (trigger == null) {
            return false;
        }
        final JobRunnerKey jobRunnerKey = JobRunnerKey.of(trigger.getJobKey().getName());
        if (deleteTrigger(jobId) && !hasAnyTriggers(jobRunnerKey)) {
            deleteJob(jobRunnerKey);
        }
        return true;
    }

    void start() throws SchedulerServiceException {
        try {
            getScheduler().start();
        } catch (SchedulerException se) {
            throw checked("Quartz scheduler refused to start", se);
        }
    }

    void standby() throws SchedulerServiceException {
        try {
            getScheduler().standby();
        } catch (SchedulerException se) {
            throw checked("Quartz scheduler refused to enter standby mode", se);
        }
    }

    void shutdown() {
        try {
            getScheduler().shutdown();
        } catch (SchedulerException se) {
            LOG.error("Error shutting down internal scheduler", se);
        }
    }

    private Scheduler getScheduler() {
        try {
            return quartzRef.get();
        } catch (LazyReference.InitializationException ex) {
            throw unchecked("Error creating underlying Quartz scheduler", ex.getCause());
        }
    }


    /**
     * Log a warning message, including the full stack trace iff DEBUG logging is enabled.
     *
     * @param message the log message, which should have exactly one {@code {}} placeholder for {@code arg}
     * @param arg     the argument for the message; typically a {@code JobId} or {@code JobRunnerKey}
     * @param e       the exception, which will be full stack traced if DEBUG logging is enabled; otherwise, appended
     *                to the message in {@code toString()} form.
     */
    private static void logWarn(String message, Object arg, Throwable e) {
        if (LOG.isDebugEnabled()) {
            LOG.warn(message, arg, e);
        } else {
            LOG.warn(message + ": {}", arg, e.toString());
        }
    }

    static TriggerKey jobId(JobId jobId) {
        return triggerKey(jobId.toString(), QUARTZ_TRIGGER_GROUP);
    }

    static JobKey jobRunnerKey(JobRunnerKey jobRunnerKey) {
        return jobKey(jobRunnerKey.toString(), QUARTZ_JOB_GROUP);
    }

    private static SchedulerServiceException checked(final String message, Throwable e) {
        return new SchedulerServiceException(message, e);
    }

    private static SchedulerRuntimeException unchecked(final String message, Throwable e) {
        return new SchedulerRuntimeException(message, e);
    }
}
